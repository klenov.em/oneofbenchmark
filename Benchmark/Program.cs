﻿using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using BenchmarkDotNet.Running;

namespace Benchmark
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // var b = new ApiBenchmark();
            // var res = await b.ExceptionApiCall();
            //
            // Console.WriteLine("Hello");
            
            BenchmarkRunner.Run<ApiBenchmark>();
        }
    }
}