using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Logging;
using OneOfService;
using OneOfService.Models;

namespace Benchmark
{
    [InProcess]
    [MemoryDiagnoser]
    public class ApiBenchmark
    {
        private readonly Random exRandom = new Random(500);
        private readonly Random erRandom = new Random(500);
        
        private static readonly WebApplicationFactory<Startup> ExceptionFactory = new WebApplicationFactory<Startup>()
            .WithWebHostBuilder(configuration =>
            {
                configuration.ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                });
            });
        
        private static readonly WebApplicationFactory<Startup> ErrorFactory = new WebApplicationFactory<Startup>()
            .WithWebHostBuilder(configuration =>
            {
                configuration.ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                });
            });

        private static readonly HttpClient ExceptionClient = ExceptionFactory.CreateClient();
        private static readonly HttpClient ErrorClient = ErrorFactory.CreateClient();

        private static readonly CreateUser ValidUser = new CreateUser {Age = 25, Email = "somerandom@ozon.ru"};
        private static readonly CreateUser InValidUserEmail = new CreateUser {Age = 25, Email = "metazon.ru"};
        private static readonly CreateUser InValidUserAge = new CreateUser {Age = 15, Email = "somerandom@ozon.ru"};

        [Benchmark]
        public async Task<HttpResponseMessage> ExceptionApiCall()
        {
            var rand = exRandom.Next();

            if (rand % 2 == 0)
            {
                return await ExceptionClient.PostAsJsonAsync("/exception-user/create-user", InValidUserEmail);
            }
            if (rand % 5 == 0)
            {
                return await ExceptionClient.PostAsJsonAsync("/exception-user/create-user", ValidUser);
            }

            return await ExceptionClient.PostAsJsonAsync("/exception-user/create-user", InValidUserAge);
        }
        
        [Benchmark]
        public async Task<HttpResponseMessage> ErrorApiCall()
        {
            var rand = erRandom.Next();

            if (rand % 2 == 0)
            {
                return await ErrorClient.PostAsJsonAsync("/one-of-user/create-user", InValidUserEmail);
            }
            if (rand % 5 == 0)
            {
                return await ErrorClient.PostAsJsonAsync("/one-of-user/create-user", ValidUser);
            }

            return await ErrorClient.PostAsJsonAsync("/one-of-user/create-user", InValidUserAge);
        }
    }
}