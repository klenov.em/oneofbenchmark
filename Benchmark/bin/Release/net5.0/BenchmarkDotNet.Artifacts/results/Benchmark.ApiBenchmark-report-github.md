``` ini

BenchmarkDotNet=v0.13.0, OS=macOS Big Sur 11.4 (20F71) [Darwin 20.5.0]
Intel Core i7-8557U CPU 1.70GHz (Coffee Lake), 1 CPU, 8 logical and 4 physical cores
.NET SDK=5.0.100
  [Host] : .NET 5.0.0 (5.0.20.51904), X64 RyuJIT

Job=InProcess  Toolchain=InProcessEmitToolchain  

```
|           Method |     Mean |   Error |  StdDev |  Gen 0 | Gen 1 | Gen 2 | Allocated |
|----------------- |---------:|--------:|--------:|-------:|------:|------:|----------:|
| ExceptionApiCall | 129.6 μs | 0.34 μs | 0.27 μs | 0.7324 |     - |     - |     28 KB |
|     ErrorApiCall | 104.3 μs | 0.40 μs | 0.31 μs | 0.7324 |     - |     - |     28 KB |
