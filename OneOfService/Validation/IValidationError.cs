namespace OneOfService.Validation
{
    public interface IValidationError
    {
        public string GetErrorMessage();
    }
}