namespace OneOfService.Validation
{
    public struct EmailNotValidError : IValidationError
    {
        public string GetErrorMessage()
        {
            return "email not valid";
        }
    }
}