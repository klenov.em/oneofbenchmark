using System.Text.RegularExpressions;

namespace OneOfService.Validation
{
    public static class UserServiceValidator
    {
        public static bool IsUserAgeApplicable(int age)
        {
            return age > 18;
        }

        public static bool IsEmailValid(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return false;
            }
            
            var regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");  
            var match = regex.Match(email);
            if (!match.Success)
            {
                return false;
            }
            
            return true;

        }
    }
}