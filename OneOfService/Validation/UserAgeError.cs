namespace OneOfService.Validation
{
    public struct UserAgeError: IValidationError
    {
        public int Expected { get; set; }
        public int Received { get; set; }
        
        public string GetErrorMessage()
        {
            return $"user age not valid. expected at least {Expected}, but received {Received}";
        }
    }
}