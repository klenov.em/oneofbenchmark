using System;

namespace OneOfService.Validation
{
    public class UserAgeException : Exception
    {
        public int Expected { get; set; }
        
        public int Received { get; set; }
    }
}