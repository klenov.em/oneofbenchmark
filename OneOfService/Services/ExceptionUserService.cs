using System;
using OneOfService.Models;
using OneOfService.Validation;

namespace OneOfService.Services
{
    public class ExceptionUserService: IExceptionUserService
    {
        public User CreateUser(CreateUser user)
        {
            if (!UserServiceValidator.IsEmailValid(user.Email))
            {
                throw new EmailNotValidException();
            }

            if (!UserServiceValidator.IsUserAgeApplicable(user.Age))
            {
                throw new UserAgeException
                {
                    Expected = 18,
                    Received = user.Age
                };
            }

            return new User
            {
                Email = user.Email,
                Age = user.Age,
                Id = Guid.NewGuid()
            };
        }
    }
}