using OneOf;
using OneOfService.Models;
using OneOfService.Validation;

namespace OneOfService.Services
{
    public interface IOneOfUserService
    {
        OneOf<User, IValidationError> CreateUser(CreateUser user);
    }
}