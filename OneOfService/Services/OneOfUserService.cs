using System;
using OneOf;
using OneOfService.Models;
using OneOfService.Validation;

namespace OneOfService.Services
{
    public class OneOfUserService: IOneOfUserService
    {
        public OneOf<User, IValidationError> CreateUser(CreateUser user)
        {
            if (!UserServiceValidator.IsEmailValid(user.Email))
            {
                return new EmailNotValidError();
            }

            if (!UserServiceValidator.IsUserAgeApplicable(user.Age))
            {
                return new UserAgeError
                {
                    Expected = 18,
                    Received = user.Age
                };
            }

            return new User
            {
                Email = user.Email,
                Age = user.Age,
                Id = Guid.NewGuid()
            };
        }
    }
}