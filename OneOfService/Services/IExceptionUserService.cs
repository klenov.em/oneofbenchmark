using OneOfService.Models;

namespace OneOfService.Services
{
    public interface IExceptionUserService
    {
        User CreateUser(CreateUser user);
    }
}