using Microsoft.AspNetCore.Mvc;
using OneOfService.Models;
using OneOfService.Services;

namespace OneOfService.Controllers
{
    [ApiController]
    [Route("one-of-user")]
    public class OneOfUserController : ControllerBase
    {
        private readonly IOneOfUserService _oneOfUserService;

        public OneOfUserController(IOneOfUserService oneOfUserService)
        {
            _oneOfUserService = oneOfUserService;
        }
        
        [HttpPost("create-user")]
        public ActionResult<User> CreateUser(CreateUser request)
        {
            var user = _oneOfUserService.CreateUser(request);

            return user.Match<ActionResult>(Ok, error => BadRequest(error.GetErrorMessage()));
        }
    }
}