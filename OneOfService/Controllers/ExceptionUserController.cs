using Microsoft.AspNetCore.Mvc;
using OneOfService.Models;
using OneOfService.Services;
using OneOfService.Validation;

namespace OneOfService.Controllers
{
    [ApiController]
    [Route("exception-user")]
    public class ExceptionUserController : ControllerBase
    {
        private readonly IExceptionUserService _exceptionUserService;

        public ExceptionUserController(IExceptionUserService exceptionUserService)
        {
            _exceptionUserService = exceptionUserService;
        }
        
        [HttpPost("create-user")]
        public ActionResult<User> CreateUser(CreateUser request)
        {
            try
            {
                return Ok(_exceptionUserService.CreateUser(request));
            }
            catch (EmailNotValidException)
            {
                return BadRequest("email not valid");
            }
            catch (UserAgeException e)
            {
                return BadRequest($"user age not valid. expected at least {e.Expected}, but received {e.Received}");
            }
        }
    }
}