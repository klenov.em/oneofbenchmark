namespace OneOfService.Models
{
    public class CreateUser
    {
        public string Email { get; set; }
        
        public int Age { get; set; }
    }
}