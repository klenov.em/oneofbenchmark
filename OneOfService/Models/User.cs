using System;

namespace OneOfService.Models
{
    public class User
    {
        public string Email { get; set; }
        
        public int Age { get; set; }
        
        public Guid Id { get; set; }
    }
}